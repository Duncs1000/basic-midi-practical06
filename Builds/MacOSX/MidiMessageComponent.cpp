//
//  MidiMessageComponent.cpp
//  JuceBasicWindow
//
//  Created by Duncan Whale on 11/13/14.
//
//

#include "MidiMessageComponent.h"

//=================================================================
MidiMessageComponent::MidiMessageComponent()
{
    setSize (200, 150);
    
    // Initialise combo box.
    messageType.addItem("Note On/Off", 1);
    messageType.addItem("Program Change", 2);
    messageType.addItem("Pitch Wheel", 3);
    messageType.addItem("Aftertouch", 4);
    messageType.addItem("Controller", 5);
    addAndMakeVisible(&messageType);
    messageType.addListener(this);
    
    // Initialise sliders.
    midiChannel.setSliderStyle(juce::Slider::IncDecButtons);
    midiChannel.setRange(1, 16, 1);
    addAndMakeVisible(&midiChannel);
    midiChannel.addListener(this);
    
    midiData1.setSliderStyle(juce::Slider::IncDecButtons);
    midiData1.setRange(0, 127, 1);
    addAndMakeVisible(&midiData1);
    midiData1.addListener(this);
    
    midiData2.setSliderStyle(juce::Slider::IncDecButtons);
    midiData2.setRange(0, 127, 1);
    addAndMakeVisible(&midiData2);
    midiData2.addListener(this);
    
    // Initialise labels.
    messageTypeLabel.setText("Message Type", dontSendNotification);
    addAndMakeVisible(&messageTypeLabel);
    
    midiChannelLabel.setText("Channel", dontSendNotification);
    addAndMakeVisible(&midiChannelLabel);
    
    midiData1Label.setText("Data 1", dontSendNotification);
    addAndMakeVisible(&midiData1Label);
    
    midiData2Label.setText("Data 2", dontSendNotification);
    addAndMakeVisible(&midiData2Label);
}

MidiMessageComponent::~MidiMessageComponent()
{
    
}

void MidiMessageComponent::resized()
{
    // Draw interface to have labels along the top, controls below and the general message description label at the bottom.
    messageTypeLabel.setBounds(10, 10, (getWidth() / 4.0) - 20, 40);
    midiChannelLabel.setBounds((getWidth() / 4.0) + 10, 10, (getWidth() / 4.0) - 20, 40);
    midiData1Label.setBounds(((getWidth() / 4.0) * 2.0) + 10, 10, (getWidth() / 4.0) - 20, 40);
    midiData2Label.setBounds(((getWidth() / 4.0) * 3.0) + 10, 10, (getWidth() / 4.0) - 20, 40);
    
    messageType.setBounds(10, 60, (getWidth() / 5.0) - 20, 40);
    midiChannel.setBounds((getWidth() / 4.0) + 10, 60, (getWidth() / 4.0) - 20, 40);
    midiData1.setBounds(((getWidth() / 4.0) * 2.0) + 10, 60, (getWidth() / 4.0) - 20, 40);
    midiData2.setBounds(((getWidth() / 4.0) * 3.0) + 10, 60, (getWidth() / 4.0) - 20, 40);
}

void MidiMessageComponent::sliderValueChanged (Slider* slider)
{
    
}

void MidiMessageComponent::comboBoxChanged (ComboBox *comboBoxThatHasChanged)
{
    if (comboBoxThatHasChanged == &messageType)
    {
        // Reset GUI based on what is required for particular message type.
        if (comboBoxThatHasChanged->getSelectedId() == 1)
        {
            midiData1.setRange(0, 127, 1);
            
            if (!midiData2.isVisible())
                midiData2.setVisible(true);
            
            if (!midiData2Label.isVisible())
                midiData2Label.setVisible(true);
            
            midiData1Label.setText("Note Number", dontSendNotification);
            midiData2Label.setText("Velocity", dontSendNotification);
        }
        else if (comboBoxThatHasChanged->getSelectedId() == 2)
        {
            midiData1.setRange(0, 127, 1);
            
            if (midiData2.isVisible())
                midiData2.setVisible(false);
            
            if (midiData2Label.isVisible())
                midiData2Label.setVisible(false);
            
            midiData1Label.setText("Program Number", dontSendNotification);
        }
        else if (comboBoxThatHasChanged->getSelectedId() == 3)
        {
            midiData1.setRange(0, 16383, 1);
            
            if (midiData2.isVisible())
                midiData2.setVisible(false);
            
            if (midiData2Label.isVisible())
                midiData2Label.setVisible(false);
            
            midiData1Label.setText("Bend Value", dontSendNotification);
        }
        else if (comboBoxThatHasChanged->getSelectedId() == 4)
        {
            midiData1.setRange(0, 127, 1);
            
            if (midiData2.isVisible())
                midiData2.setVisible(false);
            
            if (midiData2Label.isVisible())
                midiData2Label.setVisible(false);
            
            midiData1Label.setText("Aftertouch", dontSendNotification);
        }
        else if (comboBoxThatHasChanged->getSelectedId() == 5)
        {
            midiData1.setRange(0, 127, 1);
            
            if (!midiData2.isVisible())
                midiData2.setVisible(true);
            
            if (!midiData2Label.isVisible())
                midiData2Label.setVisible(true);
            
            midiData1Label.setText("Controller", dontSendNotification);
            midiData2Label.setText("Value", dontSendNotification);
        }
    }
}

MidiMessage MidiMessageComponent::getMidiMessage() const
{
    static MidiMessage midiMessage;
    
    // Set up a Midi message, based on the component values.
    if (messageType.getSelectedId() == 1)
        midiMessage = MidiMessage::noteOn (midiChannel.getValue(), midiData1.getValue(), (uint8)midiData2.getValue());
    else if (messageType.getSelectedId() == 2)
        midiMessage = MidiMessage::programChange (midiChannel.getValue(), midiData1.getValue());
    else if (messageType.getSelectedId() == 3)
        midiMessage = MidiMessage::pitchWheel (midiChannel.getValue(), midiData1.getValue());
    else if (messageType.getSelectedId() == 4)
        midiMessage = MidiMessage::channelPressureChange (midiChannel.getValue(), midiData1.getValue());
    else if (messageType.getSelectedId() == 5)
        midiMessage = MidiMessage::controllerEvent (midiChannel.getValue(), midiData1.getValue(), midiData2.getValue());

    // Return the resultant message.
    return midiMessage;
}