/*
 ==============================================================================
 
 This file was auto-generated!
 
 ==============================================================================
 */

#ifndef MAINCOMPONENT_H_INCLUDED
#define MAINCOMPONENT_H_INCLUDED

#include "../JuceLibraryCode/JuceHeader.h"
#include "MidiMessageComponent.h"


//==============================================================================
/*
 This component lives inside our window, and this is where you should put all
 your controls and content.
 */
class MainComponent   : public Component,
                        public TextButton::Listener,
                        public MidiInputCallback

{
public:
    //==============================================================================
    MainComponent();
    ~MainComponent();
    
    void resized() override;
    
    void buttonClicked (Button* button) override;
    
    void handleIncomingMidiMessage (MidiInput* source, const MidiMessage& message) override;
    
private:
    //==============================================================================
    AudioDeviceManager audioDeviceManager;

    Label midiLabel;
    TextButton sendButton;
    MidiMessageComponent midiMessageComponent;

    JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR (MainComponent);
};


#endif  // MAINCOMPONENT_H_INCLUDED
