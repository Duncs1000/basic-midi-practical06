//
//  MidiMessageComponent.h
//  JuceBasicWindow
//
//  Created by Duncan Whale on 11/13/14.
//
//

#ifndef MIDIMESSAGECOMPONENT_H
#define MIDIMESSAGECOMPONENT_H

#include "../JuceLibraryCode/JuceHeader.h"

class MidiMessageComponent    : public Component,
                                public Slider::Listener,
                                public ComboBox::Listener
{
public:
    //=================================================================
    MidiMessageComponent();
    ~MidiMessageComponent();
    
    void resized() override;
    
    void sliderValueChanged (Slider* slider) override;
    void comboBoxChanged (ComboBox *comboBoxThatHasChanged) override;
    
    MidiMessage getMidiMessage() const;
    
private:
    //=================================================================
    ComboBox messageType;
    
    Slider midiChannel;
    Slider midiData1;
    Slider midiData2;
    
    Label messageTypeLabel;
    Label midiChannelLabel;
    Label midiData1Label;
    Label midiData2Label;
};

#endif /* defined(MIDIMESSAGECOMPONENT_H) */
