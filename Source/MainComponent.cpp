/*
  ==============================================================================

    This file was auto-generated!

  ==============================================================================
*/

#include "MainComponent.h"


//==============================================================================
MainComponent::MainComponent()
{
    setSize (500, 400);
    
    // Initialise audio device manager.
    audioDeviceManager.setMidiInputEnabled ("USB Axiom 49 Port 1", true);
    audioDeviceManager.addMidiInputCallback (String::empty, this);
    audioDeviceManager.setDefaultMidiOutput ("SimpleSynth virtual input");
    
    // Initialise message label.
    midiLabel.setText("Midi Message Data.", dontSendNotification);
    addAndMakeVisible(&midiLabel);
    
    // Initialise the custom MIDI message component.
    addAndMakeVisible(&midiMessageComponent);
    
    // Initialise button.
    sendButton.setButtonText("Send");
    addAndMakeVisible(&sendButton);
    sendButton.addListener(this);

}

MainComponent::~MainComponent()
{
    audioDeviceManager.removeMidiInputCallback (String::empty, this);
}

void MainComponent::resized()
{
    midiMessageComponent.setBounds(10, 10, ((getWidth() / 5.0) * 4.0) - 20, 150);
    sendButton.setBounds((getWidth() / 5.0) * 4.0, 60, (getWidth() / 5.0) - 20, 40);
    midiLabel.setBounds(10, 110, getWidth() - 20, 40);
}

void MainComponent::buttonClicked (Button* button)
{
    // Create a MIDI message with the data dictated by the controls.
    if (&sendButton == button)
    {
        // Send the message out.
        audioDeviceManager.getDefaultMidiOutput()->sendMessageNow(midiMessageComponent.getMidiMessage());
    }
}

void MainComponent::handleIncomingMidiMessage (MidiInput* source, const MidiMessage& message)
{
    String midiText;
    
    // Set the bottom label to display the relevant MIDI data.
    if (message.isNoteOnOrOff())
    {
        midiText << "NoteOn: Channel - " << message.getChannel();
        midiText << ": Number - " << message.getNoteNumber();
        midiText << ": Velocity - " << message.getVelocity();
    }
    else if (message.isProgramChange())
    {
        midiText << "ProgramChange: Channel - " << message.getChannel();
        midiText << ": Program - " << message.getProgramChangeNumber();
    }
    else if (message.isPitchWheel())
    {
        midiText << "PitchWheel: Channel - " << message.getChannel();
        midiText << ": Value - " << message.getPitchWheelValue();
    }
    else if (message.isChannelPressure())
    {
        midiText << "Aftertouch: Channel - " << message.getChannel();
        midiText << ": Value - " << message.getChannelPressureValue();
    }
    else if (message.isController())
    {
        midiText << "ControlChange: Channel - " << message.getChannel();
        midiText << ": Controller - " << message.getControllerNumber();
        midiText << ": Value - " << message.getControllerValue();
    }
    
    // Update the label.
    midiLabel.getTextValue() = midiText;
}